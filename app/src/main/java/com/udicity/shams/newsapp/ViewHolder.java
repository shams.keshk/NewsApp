package com.udicity.shams.newsapp;

import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Shams_Keshk on 13/08/17.
 */

public class ViewHolder {

    @BindView(R.id.news_title_id)
    TextView newsTitle;
    @BindView(R.id.news_section_id)
    TextView newsSection;
    @BindView(R.id.news_date_id)
    TextView newsDate;

    public ViewHolder(View view) {
        ButterKnife.bind(this, view);
    }
}