package com.udicity.shams.newsapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.webkit.CookieManager;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WebViewActivity extends AppCompatActivity {

    private final String LOG_TAG = WebViewActivity.class.getSimpleName();
    @BindView(R.id.news_web_view_id)
    WebView webView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.fab)
    FloatingActionButton fab;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        CookieManager.getInstance().setAcceptThirdPartyCookies(webView, true);

        setWebViewSetting(webView);
        setWebViewClient(webView);

        String newsUrl = getIntent().getStringExtra("news_url");
        webView.loadUrl(newsUrl);


        onCLickFloatingButton(fab);
    }

    private void setWebViewSetting(WebView webView) {

        webView.getSettings().setJavaScriptEnabled(true);
        webView.canGoBack();
        webView.canGoForward();
        webView.getSettings().setAllowContentAccess(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(true);
        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setAllowFileAccessFromFileURLs(true);
    }

    private void setWebViewClient(WebView webView) {
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                Log.e(LOG_TAG, "error occurred " + error);
            }
        });

    }

    public void onCLickFloatingButton(FloatingActionButton fab) {
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (NetworkStatues.isConnected(WebViewActivity.this)) {
                    Intent intent = new Intent(WebViewActivity.this, MainActivity.class);
                    startActivity(intent);
                } else {
                    /*
                    Snackbar.make(view, "You Are Offline , Check Your Network", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                            */

                    Snackbar snackbar =Snackbar.make(findViewById(R.id.coordinate_layout),"You Are Offline , Check Your Network",Snackbar.LENGTH_SHORT);
                    snackbar.show();
                    snackbar.setAction("Refresh", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //Need Implementation
                        }
                    });

                }
            }
        });
    }

}
