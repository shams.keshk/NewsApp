package com.udicity.shams.newsapp;

/**
 * Created by Shams_Keshk on 13/08/17.
 */

public class NewsDescriptor {

    private String newsTitle;
    private String newsSection;
    private String newsDate;
    private String newsWebUrl;

    public NewsDescriptor(String newsTitle, String newsSection, String newsDate, String newsWebUrl) {
        this.newsTitle = newsTitle;
        this.newsSection = newsSection;
        this.newsDate = newsDate;
        this.newsWebUrl = newsWebUrl;
    }

    public String getNewsTitle() {
        return newsTitle;
    }

    public String getNewsSection() {
        return newsSection;
    }

    public String getNewsDate() {
        return newsDate;
    }

    public String getNewsWebUrl() {
        return newsWebUrl;
    }

}