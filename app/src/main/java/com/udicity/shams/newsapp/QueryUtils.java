package com.udicity.shams.newsapp;

import android.text.TextUtils;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Shams_Keshk on 13/08/17.
 */

public final class QueryUtils {

    private static String LOG_TAG = QueryUtils.class.getSimpleName();

    public QueryUtils() {
    }

    public static List<NewsDescriptor> fetchJsonData(String mUrl) {
        URL url = createUrl(mUrl);
        String jsonResponse = null;
        try {
            jsonResponse = makeHttpRequest(url);
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error Fetching Json Data", e);
        }

        return extractJsonData(jsonResponse);
    }

    private static URL createUrl(String mUrl) {
        URL url = null;
        try {
            url = new URL(mUrl);
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Invalid Url : ", e);
        }
        return url;
    }

    private static String makeHttpRequest(URL url) throws IOException {
        String jsonResponse = "";
        if (url == null) {
            return jsonResponse;
        }

        HttpURLConnection httpsURLConnection = null;
        InputStream inputStream = null;

        try {
            httpsURLConnection = (HttpURLConnection) url.openConnection();
            httpsURLConnection.setConnectTimeout(15000);
            httpsURLConnection.setReadTimeout(10000);
            httpsURLConnection.setRequestMethod("GET");
            httpsURLConnection.connect();

            if (httpsURLConnection.getResponseCode() == 200) {
                inputStream = httpsURLConnection.getInputStream();
                jsonResponse = readFromInputStream(inputStream);
            } else if (httpsURLConnection.getResponseCode() == 400) {
                Log.e(LOG_TAG, "Data Not Found : " + httpsURLConnection.getResponseCode());
            } else {
                Log.e(LOG_TAG, "Http Connection Error :  " + httpsURLConnection.getResponseCode());
            }

        } catch (IOException e) {
            Log.e(LOG_TAG, "http connection error ", e);
        } finally {
            if (httpsURLConnection != null) {
                httpsURLConnection.disconnect();
            }
            if (inputStream != null) {
                inputStream.close();
            }
        }
        return jsonResponse;
    }

    private static String readFromInputStream(InputStream inputStream) throws IOException {
        StringBuilder stringBuilder = new StringBuilder();
        if (inputStream != null) {
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream, Charset.forName("UTF-8"));
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            String line = bufferedReader.readLine();
            while (line != null) {
                stringBuilder.append(line);
                line = bufferedReader.readLine();
            }
        }
        return stringBuilder.toString();
    }

    private static List<NewsDescriptor> extractJsonData(String jsonResponse) {
        List<NewsDescriptor> newsDescriptorList = new ArrayList<>();

        if (TextUtils.isEmpty(jsonResponse)) {
            return null;
        }

        try {
            JSONObject jsonObject = new JSONObject(jsonResponse);
            JSONObject responseJsonObject = new JSONObject();
            if (jsonObject.has("response")) {
                responseJsonObject = jsonObject.getJSONObject("response");
            }
            JSONArray jsonArray = new JSONArray();
            if (responseJsonObject.has("results")) {
                jsonArray = responseJsonObject.getJSONArray("results");
            }
            String newsTitle = "";
            String newsSection = "";
            String newsDate = "";
            String newsWebUrl = "";
            for (int i = 0; i < jsonArray.length(); i++) {
                if (jsonArray.getJSONObject(i).has("webTitle")) {
                    newsTitle = jsonArray.getJSONObject(i).getString("webTitle");
                }
                if (jsonArray.getJSONObject(i).has("sectionName")) {
                    newsSection = jsonArray.getJSONObject(i).getString("sectionName");
                }
                if (jsonArray.getJSONObject(i).has("webPublicationDate")) {
                    newsDate = jsonArray.getJSONObject(i).getString("webPublicationDate");
                }
                if (jsonArray.getJSONObject(i).has("webUrl")) {
                    newsWebUrl = jsonArray.getJSONObject(i).getString("webUrl");
                }

                newsDescriptorList.add(new NewsDescriptor(newsTitle, newsSection, newsDate, newsWebUrl));
            }
        } catch (JSONException e) {
            Log.e(LOG_TAG, "Error While Parsing Json Response");
        }

        return newsDescriptorList;
    }
}
