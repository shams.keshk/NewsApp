package com.udicity.shams.newsapp;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Shams_Keshk on 13/08/17.
 */

public class NewsAdapter extends ArrayAdapter<NewsDescriptor> {

    private String LOG_TAG = NewsAdapter.class.getSimpleName();

    public NewsAdapter(Activity activity, ArrayList<NewsDescriptor> newsDescriptorArrayList) {
        super(activity, 0, newsDescriptorArrayList);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.news_list_item, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        NewsDescriptor currentNews = getItem(position);

        assert currentNews != null;

        String newsDate = currentNews.getNewsDate();
        String dateString = "";
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.UK);
        try {
            Date date = format.parse(newsDate);
            dateString = formatDate(date);
        } catch (ParseException e) {
            Log.e(LOG_TAG, "error while parsing date : ", e);
        }

        viewHolder.newsTitle.setText(currentNews.getNewsTitle());
        viewHolder.newsSection.setText(currentNews.getNewsSection());
        viewHolder.newsDate.setText(dateString);

        return convertView;
    }

    private String formatDate(Date date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM yyyy hh:mm a", Locale.UK);
        return simpleDateFormat.format(date);
    }

}
