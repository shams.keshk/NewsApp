package com.udicity.shams.newsapp;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Shams_Keshk on 29/09/17.
 */

public class NewsRecyclerAdapter extends RecyclerView.Adapter<NewsRecyclerAdapter.NewsViewHolder> {

    private String LOG_TAG = NewsRecyclerAdapter.class.getSimpleName();

    private List<NewsDescriptor> newsDescriptor;

    private ListItemClickListener listItemClickListener;

    public void setNewsDescriptor(List<NewsDescriptor> newsDescriptor) {
        this.newsDescriptor = newsDescriptor;
    }

    public NewsRecyclerAdapter(List<NewsDescriptor> newsDescriptor , ListItemClickListener listItemClickListener) {
        this.newsDescriptor = newsDescriptor ;
        this.listItemClickListener = listItemClickListener;
    }

    @Override
    public NewsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        int itemLayoutResourceId = R.layout.news_list_item;
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(itemLayoutResourceId , parent , false);

        return new NewsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(NewsViewHolder holder, int position) {
        NewsDescriptor currentNewsItem = newsDescriptor.get(position);

        String newsDate = currentNewsItem.getNewsDate();
        String dateString = "";
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.UK);
        try {
            Date date = format.parse(newsDate);
            dateString = formatDate(date);
        } catch (ParseException e) {
            Log.e(LOG_TAG, "error while parsing date : ", e);
        }

        holder.newsTitle.setText(currentNewsItem.getNewsTitle());
        holder.newsSection.setText(currentNewsItem.getNewsSection());
        holder.newsDate.setText(dateString);

    }

    private String formatDate(Date date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM yyyy hh:mm a", Locale.UK);
        return simpleDateFormat.format(date);
    }

    @Override
    public int getItemCount() {
        return newsDescriptor.size();
    }

    public void clear() {
        int size = this.newsDescriptor.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                this.newsDescriptor.remove(0);
            }

            this.notifyItemRangeRemoved(0, size);
        }
    }

    public interface ListItemClickListener{
        void onListItemClicked(NewsDescriptor newsDescriptor);
    }

    public class NewsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        @BindView(R.id.news_title_id)
        TextView newsTitle;
        @BindView(R.id.news_section_id)
        TextView newsSection;
        @BindView(R.id.news_date_id)
        TextView newsDate;

        public NewsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this , itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int itemPosition = getAdapterPosition();
            NewsDescriptor currentNewsItem = newsDescriptor.get(itemPosition);
            listItemClickListener.onListItemClicked(currentNewsItem);
        }
    }
}
