package com.udicity.shams.newsapp;

import android.app.LoaderManager;
import android.content.Intent;
import android.content.Loader;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<List<NewsDescriptor>> , NewsRecyclerAdapter.ListItemClickListener {

    @BindView(R.id.empty_text_view)
    TextView emptyTextView;
    /*
    @BindView(R.id.news_list_view_id)
    ListView listView;
    */
    @BindView(R.id.news_recycler_list_view_id)
    RecyclerView recyclerView;
    @BindView(R.id.swiperefresh)
    SwipeRefreshLayout swipeRefreshLayout;

    private String newsApiUrl = "http://content.guardianapis.com/search?q=debates";
    private final int NEWS_LOADER_ID = 1;
    private final String API_KEY = "api-key";
    private final String API_KEY_VALUE = "e9e87560-795c-412d-a172-a92a9a95bb93";
   // private NewsAdapter newsAdapter;
    private NewsRecyclerAdapter newsRecyclerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        final LoaderManager loaderManager = getLoaderManager();

        newsRecyclerAdapter = new NewsRecyclerAdapter(new ArrayList<NewsDescriptor>() ,this);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);

        recyclerView.setLayoutManager(linearLayoutManager);

        recyclerView.setHasFixedSize(true);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        recyclerView.setAdapter(newsRecyclerAdapter);

        /*

        newsAdapter = new NewsAdapter(MainActivity.this, new ArrayList<NewsDescriptor>());

        listView.setAdapter(newsAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                NewsDescriptor currentNews = newsAdapter.getItem(position);
                Intent intent = new Intent(MainActivity.this, WebViewActivity.class);
                String newsUrl = currentNews.getNewsWebUrl();
                intent.putExtra("news_url", newsUrl);
                startActivity(intent);
            }
        });



        listView.setEmptyView(emptyTextView);

        */

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                loaderManager.restartLoader(NEWS_LOADER_ID, null ,MainActivity.this);
                progressBarState(false);
            }
        });

        if (NetworkStatues.isConnected(this)) {
            connectionTextState(false, emptyTextView);

            loaderManager.initLoader(NEWS_LOADER_ID, null, MainActivity.this);
        } else {
            connectionTextState(true, emptyTextView);
        }

    }

    @Override
    public void onListItemClicked(NewsDescriptor newsDescriptor) {
        Intent intent = new Intent(MainActivity.this, WebViewActivity.class);
        String newsUrl = newsDescriptor.getNewsWebUrl();
        intent.putExtra("news_url", newsUrl);
        startActivity(intent);
    }

    @Override
    public Loader<List<NewsDescriptor>> onCreateLoader(int id, Bundle args) {
        progressBarState(true);

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String orderBy = sharedPreferences.getString(getString(R.string.order_by_key), getString(R.string.order_by_default_value));

        String section = sharedPreferences.getString(getString(R.string.section_key), getString(R.string.section_default_value));

        Uri baseUrl = Uri.parse(newsApiUrl);
        Uri.Builder builder = baseUrl.buildUpon();
        builder.appendQueryParameter(API_KEY,API_KEY_VALUE);

        builder.appendQueryParameter("order-by", orderBy);
        if (!section.isEmpty()) {
            builder.appendQueryParameter("section", section);
        }
        return new NewsAsyncTaskLoader(getApplicationContext(), builder.toString());
    }

    @Override
    public void onLoadFinished(Loader<List<NewsDescriptor>> loader, List<NewsDescriptor> data) {

        swipeRefreshLayout.setRefreshing(false);

        if (NetworkStatues.isConnected(this)) {
            emptyTextView.setText(R.string.no_news_were_found);
        } else {
            connectionTextState(true, emptyTextView);
        }
        progressBarState(false);
      //  newsRecyclerAdapter.clear();
      //  newsAdapter.clear();

        if (data != null && !data.isEmpty()) {
           newsRecyclerAdapter.setNewsDescriptor(data);
           newsRecyclerAdapter.notifyItemInserted(data.size());
           // newsAdapter.addAll(data);
        }
    }

    @Override
    public void onLoaderReset(Loader<List<NewsDescriptor>> loader) {
          newsRecyclerAdapter.clear();
       // newsAdapter.clear();
    }

    private void progressBarState(boolean setVisibility) {
        View progressBar = findViewById(R.id.progress_bar_id);
        if (setVisibility) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }

    private void connectionTextState(boolean setVisibility, TextView emptyTextView) {
        if (setVisibility) {
            emptyTextView.setText(R.string.no_internet_connection);
            emptyTextView.setVisibility(View.VISIBLE);
        } else {
            emptyTextView.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.filter_id) {
            Intent intent = new Intent(this, SettingActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}