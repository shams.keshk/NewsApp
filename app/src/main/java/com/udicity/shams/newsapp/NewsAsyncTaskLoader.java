package com.udicity.shams.newsapp;

import android.content.AsyncTaskLoader;
import android.content.Context;

import java.util.List;

/**
 * Created by Shams_Keshk on 13/08/17.
 */

public class NewsAsyncTaskLoader extends AsyncTaskLoader<List<NewsDescriptor>> {

    private String url;

    public NewsAsyncTaskLoader(Context context, String mUrl) {
        super(context);
        url = mUrl;
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }

    @Override
    public List<NewsDescriptor> loadInBackground() {

        if (url == null) {
            return null;
        }
        return QueryUtils.fetchJsonData(url);
    }
}
